#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch Import Line"
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, If, In
from trytond.transaction import Transaction
from trytond.pool import Pool


_ZERO = Decimal("0.0")

class Line(ModelSQL, ModelView):
    'Account Batch Entry Import Line'
    _name = 'account.batch.import.line'
    _description = __doc__
    _rec_name = 'description'

    posting_text = fields.Char('Posting Text')
    batch = fields.Many2One('account.batch', 'Batch Entry',
            required=True, ondelete='CASCADE')
    journal = fields.Many2One('account.batch.journal', 'Journal',
            required=True, states={'readonly': True}, select=1)
    fiscalyear = fields.Function(fields.Many2One('account.fiscalyear',
            'Fiscal Year', depends=['date']), 'get_fiscalyear',
            setter='set_fiscalyear')
    date = fields.Date('Date', required=True, on_change=[
                    'date', 'fiscalyear', 'account',
                    'contra_account', 'journal', 'amount',
                    ])
    amount = fields.Numeric('Amount', required=True,
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'])
    account = fields.Many2One('account.account', 'Account', required=True,
            domain=[('kind', '!=', 'view')], states={'readonly': True})
    contra_account = fields.Many2One('account.account', 'Contra Account',
            required=True,
            domain=[('kind', '!=', 'view')],
            on_change=['contra_account', 'account'])
    tax = fields.Many2One('account.tax', 'Tax')
    side_account = fields.Function(fields.Selection([
                ('', ''),
                ('debit', 'D'),
                ('credit', 'C'),
                ], ' ', readonly=True, on_change_with=[
                'amount', 'journal',
                ],
                help="'D': Account is debit side\n" \
                    "'C': Account is credit side"),
                    'get_side_account')
    side_contra_account = fields.Function(fields.Selection([
                ('', ''),
                ('debit', 'D'),
                ('credit', 'C'),
                ], ' ', readonly=True, on_change_with=[
                'amount', 'journal',
                ],
                help="'D': Contra Account is debit side\n" \
                    "'C': Contra Account is credit side"),
                    'get_side_contra_account')
    party = fields.Many2One('party.party', 'Party',
            on_change=['amount', 'party', 'journal', 'account', 'date',
                    'contra_account', 'is_cancelation_move', 'invoice']
            )
    is_cancelation_move = fields.Boolean('Cancelation',
            states={'readonly': Bool(Eval('invoice'))}, depends=['invoice'])
    external_reference = fields.Char('External Reference', help='This field '
            'collects references to external documents, like voucher or '
            'receipt numbers.')
    currency_digits = fields.Function(fields.Integer('Currency Digits',
        on_change_with=['journal']), 'get_currency_digits')
    invoice = fields.Many2One('account.invoice', 'Invoice',
            domain=[
                ('party', If(Bool(Eval('party')), '=', '!='), Eval('party')),
                ('state', 'in',
                    If(
                        In(Eval('state'), ['new', 'draft']),
                        ['open', ], ['open', 'paid']
                    )
                ),
            ],
            on_change=['invoice', 'date', 'amount', 'journal'],
            depends=['party', 'state'])
    maturity_date = fields.Date('Maturity Date', help='Date to pay the '
            'amount of the batch line at least.')
    bank_imp_line = fields.Many2One('banking.import.line',
            'Bank Import Line', states={'readonly': True})

    def __init__(self):
        super(Line, self).__init__()
        self._error_messages.update({
            'same_account_and_contra': 'Account and contra account are ' \
                'equal!\nPlease provide different accounts.',
            })

    def default_fiscalyear(self):
        pool = Pool()
        date_obj = pool.get('ir.date')
        fiscalyear_obj = pool.get('account.fiscalyear')
        date = date_obj.today()
        company_id = Transaction().context.get('company', False)
        fiscalyear_id = fiscalyear_obj.find(company_id, date=date,
                exception=False)
        return fiscalyear_id

    def default_currency_digits(self):
        batch_obj = Pool().get('account.batch')
        if Transaction().context.get('batch'):
            batch = batch_obj.browse(Transaction().context['batch'])
            currency_digits = batch.journal.currency.digits
            return currency_digits
        return 2

    def default_side_account(self):
        return ''

    def default_get(self, fields, with_rec_name=True):
        pool = Pool()
        batch_obj = pool.get('account.batch')
        batch_journal_obj = pool.get('account.batch.journal')
        batch_user_obj = pool.get('account.batch.user')

        res = super(Line, self).default_get(fields,
                with_rec_name=with_rec_name)

        context_batch = Transaction().context.get('batch', False)
        if not context_batch:
            batch_id = batch_user_obj.search_read([
                    ('user', '=', Transaction().user)
                    ], fields_names=['batch'])
            context_batch = batch_id[0]['batch']

        if context_batch and 'batch' in fields:
            res['batch'] = context_batch
            batch = batch_obj.browse(context_batch)
            batch_journal_id = batch.journal.id
            if batch_journal_id and 'journal' in fields:
                res['journal'] = batch_journal_id
                if 'account' in fields:
                    batch_journal = batch_journal_obj.browse(
                            batch_journal_id).account_journal
                    account, side = self._choose_account(_ZERO, batch_journal)
                    res['account'] = account
                if 'side_account' in fields:
                    res['side_account'] = side
                if 'side_contra_account' in fields:
                    res['side_contra_account'] = self._opposite(side)
        return res

    def _opposite(self, side):
        opposite = ''
        if side == 'debit':
            opposite='credit'
        elif side == 'credit':
            opposite = 'debit'
        return opposite

    def on_change_with_side_contra_account(self, values):
        batch_journal_obj = Pool().get('account.batch.journal')
        amount = values.get('amount', _ZERO)
        batch_journal_id = values.get('journal' or False)
        account_journal = batch_journal_obj.browse(
                batch_journal_id).account_journal
        side =''
        if amount and account_journal:
            _, side = self._choose_account(amount, account_journal)
        return self._opposite(side)

    def on_change_with_side_account(self, values):
        batch_journal_obj = Pool().get('account.batch.journal')
        amount = values.get('amount', _ZERO)
        batch_journal_id = values.get('journal' or False)
        account_journal = batch_journal_obj.browse(
                batch_journal_id).account_journal
        side = ''
        if amount and account_journal:
            _, side = self._choose_account(amount, account_journal)
        return side

    def _choose_account(self, amount, account_journal):
        if amount >= _ZERO:
            account = account_journal.credit_account.id
            side = 'credit'
        else:
            account = account_journal.debit_account.id
            side = 'debit'

        if account_journal.type in ['revenue', 'cash', 'bank']:
            if amount >= _ZERO:
                account = account_journal.debit_account.id
                side = 'debit'
            else:
                account = account_journal.credit_account.id
                side = 'credit'
        return account, side

    def on_change_contra_account(self, values):
        account_obj = Pool().get('account.account')
        account = values.get('account')
        contra_account = values.get('contra_account')
        res = {}
        if account == contra_account:
            res['contra_account'] = False
        if contra_account:
            account = account_obj.browse(contra_account)
            if account.taxes and len(account.taxes)==1:
                res['tax'] = account.taxes[0].id
        return res

    def on_change_with_currency_digits(self, values):
        journal_obj = Pool().get('account.batch.journal')

        if values.get('journal'):
            journal = journal_obj.browse(values['journal'])
            return journal.currency.digits

        return 2

    def on_change_party(self, values):
        pool = Pool()
        party_obj = pool.get('party.party')
        batch_journal_obj = pool.get('account.batch.journal')
        invoice_obj = pool.get('account.invoice')

        res = {}
        party_id = values.get('party')
        batch_journal_id = values.get('journal')
        invoice_id = values.get('invoice')
        amount = values.get('amount')
        if party_id and batch_journal_id:
            party = party_obj.browse(party_id)
            batch_journal = batch_journal_obj.browse(batch_journal_id)
            if amount:
                if batch_journal.account_journal.type == 'expense':
                    res['account'] = party.account_payable.id
                if batch_journal.account_journal.type == 'revenue':
                    res['account'] = party.account_receivable.id
                if batch_journal.account_journal.type in ['cash', 'bank']:
                    if amount >= _ZERO:
                        res['contra_account'] = party.account_receivable.id
                    else:
                        res['contra_account'] = party.account_payable.id
        if invoice_id:
            invoice = invoice_obj.browse(invoice_id)
            if party_id and party_id != invoice.party.id:
                res['invoice'] = False
                res['contra_account'] = False
                res['external_reference'] = False
            elif not party_id:
                res['invoice'] = False
                res['external_reference'] = False
            else:
                date = values.get('date', None)
                res['contra_account'] = self._get_invoice_account_id(
                        invoice.account.id, date)

        return res

    def on_change_date(self, values):
        pool = Pool()
        batch_journal_obj = pool.get('account.batch.journal')
        fiscalyear_obj = pool.get('account.fiscalyear')
        date_obj = pool.get('ir.date')

        today = date_obj.today()
        date = values.get('date', today)
        fiscalyear_id = values.get('fiscalyear')
        batch_journal_id = values.get('journal')
        res = {}
        company_id = Transaction().context.get('company')
        new_fiscalyear_id = fiscalyear_obj.find(company_id, date=date,
                exception=True)
        if new_fiscalyear_id != fiscalyear_id and batch_journal_id:
            res['fiscalyear'] = new_fiscalyear_id
            account_journal = batch_journal_obj.browse(
                    batch_journal_id).account_journal
            new_account_id, side = self._choose_account(_ZERO, account_journal)
            res['account'] = new_account_id
            res['contra_account'] = False
        return res

    def on_change_invoice(self, values):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.batch.journal')
        currency_obj = pool.get('currency.currency')
        res = {}
        date = values.get('date', None)
        amount = values.get('amount')
        if values.get('invoice'):
            res['is_cancelation_move'] = False
            invoice = invoice_obj.browse(values['invoice'])
            journal = journal_obj.browse(values['journal'])
            amount_to_pay = currency_obj.compute(
                invoice.currency.id, invoice.amount_to_pay, journal.currency.id)
            if not amount or abs(amount) > amount_to_pay:
                res['amount'] = amount_to_pay
            if invoice.type in ['in_credit_note', 'out_credit_note']:
                if res.get('amount'):
                    res['amount'] = res['amount'] * -1
                else:
                    res['amount'] = amount * -1
            res['party'] = invoice.party.id
            res['contra_account'] = self._get_invoice_account_id(
                    invoice.account.id, date)
            if invoice.type in ['out_invoice', 'out_credit_note']:
                res['external_reference'] = invoice.number
            elif invoice.type in ['in_invoice', 'in_credit_note']:
                res['external_reference'] = invoice.external_reference
        else:
            res['external_reference'] = False
        return res

    def _get_invoice_account_id(self, invoice_id, date):
        return invoice_id

    def get_currency_digits(self, ids, names):
        res = {}
        for line in self.browse(ids):
            for name in names:
                res.setdefault(name, {})
                res[name].setdefault(line.id, 2)
                if name == 'currency_digits':
                    res[name][line.id] = line.account.currency_digits
        return res

    def get_fiscalyear(self, ids, name):
        fiscalyear_obj = Pool().get('account.fiscalyear')
        res = {}
        for line in self.browse(ids):
            company_id = Transaction().context.get('company')
            fiscalyear_id = fiscalyear_obj.find(company_id, date=line.date,
                    exception=True)
            res[line.id] = fiscalyear_id
        return res

    def set_fiscalyear(self, vals, name):
        pass

    def get_side_account(self, ids, name):
        res = {}.fromkeys(ids, '')
        for line in self.browse(ids):
            if not line.account:
                continue
            amount = line.amount or _ZERO
            account_journal = line.journal.account_journal
            _, side = self._choose_account(amount, account_journal)
            res[line.id] = side
        return res

    def get_side_contra_account(self, ids, name):
        res = {}.fromkeys(ids, '')
        for line in self.browse(ids):
            if not line.contra_account:
                continue
            amount = line.amount or _ZERO
            account_journal = line.journal.account_journal
            _, side = self._choose_account(amount, account_journal)
            res[line.id] = self._opposite(side)
        return res

Line()
