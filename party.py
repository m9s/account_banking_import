#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Bool, Eval

STATES = {
    'readonly': Not(Bool(Eval('active'))),
}
DEPENDS = ['active']


class Party(ModelSQL, ModelView):
    "Party"
    _name = "party.party"

    bank_imports = fields.Many2Many('party.party-account.banking.import',
            'party', 'account.banking.import', 'Bank Imports', states=STATES,
            depends=DEPENDS)

Party()


class PartyBankImport(ModelSQL):
    'Party - Bank Import'
    _name = 'party.party-account.banking.import'
    _table = 'party_bank_import_rel'
    _description = __doc__
    party = fields.Many2One('party.party', 'Party', ondelete='CASCADE',
            required=True, select=1)
    bank_import = fields.Many2One('account.banking.import', 'Bank Import',
            ondelete='CASCADE', required=True, select=1)

PartyBankImport()
