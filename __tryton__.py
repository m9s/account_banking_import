# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Bank Import',
    'name_de_DE': 'Buchhaltung Bank Import',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Bank Import and Payment Reconciliation
    - Provides the import of transactions from bank accounts
    - Provides reconciliation of payments with invoices
    - Creation of reconciliation drafts is performed in a automatic way.

    This module makes usage of import methods, that are defined in other
    submodules. So this module needs the additional installation of at least one
    import method module to be able to work.
''',
    'description_de_DE': '''Bankimport und Zahlungsabgleich
    - Ermöglicht den Import von Transaktionen eines Bankkontos
    - Ermöglicht den Abgleich von Zahlungen mit Rechnungen
    - Vorschläge für den Zahlungsabgleich werden automatisch erstellt

    Dieses Modul macht Gebrauch von Importmethoden, die durch andere
    Untermodule bereitgestellt werden. Dieses Modul erfordert also die
    zusätzliche Installation mindestens eines Moduls mit einer Importmethode.
''',
    'depends': [
        'account_batch_invoice',
        'account_batch_tax',
        'company',
        'party_bank',
    ],
    'xml': [
        'company.xml',
        'banking_import.xml',
        'batch.xml',
        'batch_import.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
