#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Not, Bool, Eval

STATES = {
    'readonly': Not(Bool(Eval('active'))),
}
DEPENDS = ['active']


class Company(ModelSQL, ModelView):
    "Company"
    _name = "company.company"

    bank_import_configs = fields.One2Many('banking.import.configuration',
            'company', 'Bank Import Configurations', states=STATES,
            depends=DEPENDS)

Company()
