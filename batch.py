#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
"Batch"
from trytond.model import ModelView, ModelSQL, fields

class Line(ModelSQL, ModelView):
    'Account Batch Entry Line'
    _name = 'account.batch.line'

    bank_imp_line = fields.Many2One('banking.import.line',
            'Banking Import Line', readonly=True)


Line()
