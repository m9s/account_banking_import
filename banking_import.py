# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"Banking Import"
import datetime
import base64
from decimal import Decimal
import logging
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Equal, Eval, Not, Bool, Get, PYSONEncoder
from trytond.backend import TableHandler
from trytond.transaction import Transaction
from trytond.pool import Pool


_ENC_SEL = [
            ('utf-8', 'UTF-8'),
            ('windows-1252', 'Windows-1252'),
            ]

HAS_CHARDET = False
try:
    import chardet
    HAS_CHARDET = True
    _ENC_SEL.append(('auto', 'Automatic'))
except ImportError:
    logging.getLogger('account.banking.import').warning(
        'Unable to import chardet. Autodetection of file encoding disabled.')

_ZERO = Decimal("0.0")
_STATES = {'readonly': Bool(Eval('lines'))}
_DEPENDS = ['lines']
_STATESF = {'readonly': Not(Equal(Eval('state'), 'draft'))}
_DEPENDSF = ['state']


class BankingImportConfiguration(ModelSQL, ModelView):
    'Bank Import Configuration'
    _name = 'banking.import.configuration'
    _description = __doc__

    name = fields.Char('Name', size=None, required=True, translate=True,
        loading='lazy')
    company = fields.Many2One('company.company', 'Company', required=True,
            states=_STATES, depends=_DEPENDS, select=1)
    bank_account = fields.Many2One('bank.account', 'Bank Account',
            context={'company_bank': Eval('company')},
            required=True, states=_STATES, depends=_DEPENDS,
            help='Bank Account of the company, for which the transactions ' \
                    'will be imported.')
    journal = fields.Many2One('account.batch.journal', 'Journal',
            required=True, states=_STATES, depends=_DEPENDS, select=1,
            domain=[('account_journal.type', '=', 'bank')],
            help='Bank Journal used for this import configuration.')
    import_method = fields.Selection([], 'Import Method', sort=True,
            required=True, states=_STATES, depends=_DEPENDS, select=1,
            help='Import Method to use for this configuration.\n' \
                '(Import Methods are provided by specific Submodules)')
    encoding = fields.Selection(_ENC_SEL, 'File encoding', sort=True,
            help='Charset encoding of the import file')
    lines = fields.One2Many('banking.import.line', 'bank_import_config',
            'Imported Transactions', readonly=True)
    entry_date = fields.Selection([
            ('date', 'Date'),
            ('vdate', 'Valuta Date'),
            ], 'Entry date', sort=True,
        help='The date of the bank statement, that shall be used as entry date for accounting.')
    active = fields.Boolean('Active', select=1)

    def __init__(self):
        super(BankingImportConfiguration, self).__init__()
        self._constraints += [
            ('_check_currency', 'wrong_currency'),
            ('_check_bank_account', 'bank_account_used'),
            ('_check_journal', 'journal_used'),
        ]
        self._error_messages.update({
            'wrong_currency': 'Currency of bank account and journal must be ' \
                    'the same!',
            'missing_currency_account': 'Currency on bank account is missing!',
            'bank_account_used': 'A bank account can only be used in one ' \
                'active configuration!',
            'journal_used': 'A journal can only be used in one active ' \
                'configuration!',
            })

    def default_company(self):
        return Transaction().context.get('company') or False

    def default_bank_account(self):
        company_obj = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = company_obj.browse(Transaction().context['company'])
            if len(company.bank_accounts) == 1:
                return company.bank_accounts[0].id
        return {}

    def default_encoding(self):
        if HAS_CHARDET:
            return 'auto'
        return 'utf-8'

    def default_entry_date(self):
        return 'date'

    def default_active(self):
        return True

    def _check_currency(self, ids):
        for configuration in self.browse(ids):
            if not configuration.bank_account.currency:
                self.raise_user_error('missing_currency_account')
                return False
            if configuration.bank_account.currency != \
                    configuration.journal.currency:
                self.raise_user_error('wrong_currency')
                return False
        return True

    def _check_bank_account(self, ids):
        for configuration in self.browse(ids):
            configuration_ids = self.search([
                        ('bank_account', '=', configuration.bank_account),
                        ('active', '=', True),
                    ])
            if len(configuration_ids) > 1:
                return False
        return True

    def _check_journal(self, ids):
        for configuration in self.browse(ids):
            configuration_ids = self.search([
                        ('journal.account_journal', '=',
                            configuration.journal.account_journal),
                        ('active', '=', True),
                    ])
            if len(configuration_ids) > 1:
                return False
        return True

    def _get_entry_date(self, bank_imp_line):
        bank_imp_config = self.browse(bank_imp_line.bank_import_config.id)
        if bank_imp_config.entry_date == 'date':
            entry_date = bank_imp_line.date or None
        else:
            entry_date = bank_imp_line.valuta_date or None
        return entry_date

BankingImportConfiguration()


class BankingImportLine(ModelSQL, ModelView):
    'Bank Import Line'
    _name = 'banking.import.line'
    _description = __doc__

    bank_import_config = fields.Many2One('banking.import.configuration',
            'Bank Import Configuration', required=True, ondelete='RESTRICT',
            readonly=True)
    date = fields.Date('Date', required=True, readonly=True)
    valuta_date = fields.Date('Valuta Date', required=True, readonly=True)
    account_recipient = fields.Char('Account Number Recipient', readonly=True,
            states={
                'required': Not(Bool(Eval('iban'))),
                }, depends=['iban'],
            help='National Standard Code of the Recipient')
    iban = fields.Char('IBAN', readonly=True,
            states={
                'required': Not(Bool(Eval('account_recipient'))),
                }, depends=['account_recipient'],
            help='International Bank Account Number')
    bank_code_recipient = fields.Char('Bank Code Recipient', readonly=True,
            states={
                'required': Not(Bool(Eval('bic'))),
                }, depends=['bic'],
            help='National Bank Code of the Recipient')
    bic = fields.Char('BIC/SWIFT', readonly=True,
            states={
                'required': Not(Bool(Eval('bank_code_recipient'))),
                }, depends=['bank_code_recipient'],
            help='Bank Identifier Code/Society ' \
            'for Worldwide Interbank Financial Telecommunication Number')
    account_payer = fields.Char('Account Number Payer', readonly=True,
            help='National Standard Code of the Payer')
    bank_code_payer = fields.Char('Bank Code Payer', readonly=True,
            help='National Bank Code of the Payer')
    amount = fields.Numeric('Amount', digits=(16, 2), required=True,
            readonly=True)
    currency = fields.Many2One('currency.currency', 'Currency', required=True,
            readonly=True)
    purpose = fields.Text('Purpose', readonly=True)
    start_balance = fields.Numeric('Start Balance', digits=(16, 2),
            required=True, readonly=True)
    end_balance = fields.Numeric('End Balance', digits=(16, 2), required=True,
            readonly=True)

    def __init__(self):
        super(BankingImportLine, self).__init__()
        self._error_messages.update({
            'missing_journal_accounts': 'Missing accounts on the ' \
                    'Bank Import Journal.\n' \
                    'Please configure the journal before proceeding.',
            'wrong_balance': 'Different balance between Bank Import ' \
                    'Journal and Bank Import File!\n' \
                    'Do you really want to continue?',
            })

    def get_rec_name(self, ids, name):
        pool = Pool()
        bank_imp_config_obj = pool.get('banking.import.configuration')
        lang_obj = pool.get('ir.lang')

        if not ids:
            return {}

        for code in [Transaction().context.get('language', False)
                    or 'en_US', 'en_US']:
            lang_ids = lang_obj.search([
                ('code', '=', code),
                ])
            if lang_ids:
                break
        lang = lang_obj.browse(lang_ids[0])

        res = {}
        for line in self.browse(ids):
            entry_date = bank_imp_config_obj._get_entry_date(line)
            res[line.id] = ", ".join(x for x in [
                entry_date.strftime(str(lang.date)),
                line.account_payer, line.bank_code_payer] if x)
        return res

    def search_rec_name(self, name, clause):
        ids = self.search([
            ('account_payer',) + tuple(clause[1:]),
            ], limit=1)
        if ids:
            return [('account_payer',) + tuple(clause[1:])]
        else:
            ids = self.search([
                ('bank_code_payer',) + tuple(clause[1:]),
                ], limit=1)
            if ids:
                return [('bank_code_payer',) + tuple(clause[1:])]
            else:
                ids = self.search([
                    ('bic',) + tuple(clause[1:]),
                    ], limit=1)
                if ids:
                    return [('bic',) + tuple(clause[1:])]
        return [(self._rec_name,) + tuple(clause[1:])]

    def _get_batch_import_line(self, line, batch_id, amount, invoice_id=False):
        '''
        Return values for a batch line

        :param line: the BrowseRecord of the Banking Import Line
        :param batch_id: the batch id
        :param amount: the amount as Decimal
        :param invoice_id: the invoice_id the batch line should be linked to

        :return: a dictionary with batch fields as keys
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        fiscalyear_obj = pool.get('account.fiscalyear')
        bank_imp_config_obj = pool.get('banking.import.configuration')
        batch_imp_line_obj = pool.get('account.batch.import.line')
        date_obj = pool.get('ir.date')

        today = date_obj.today()
        party_id = False
        contra_account = False
        invoice_number = False
        tax_id = False

        entry_date = bank_imp_config_obj._get_entry_date(line) or today
        if invoice_id:
            invoice = invoice_obj.browse(invoice_id)
            invoice_number = invoice.number
            party_id = invoice.party.id
            contra_account = batch_imp_line_obj._get_invoice_account_id(
                    invoice.account.id, entry_date)
            tax_id = self._get_tax(invoice_id, entry_date)

        company_id = Transaction().context.get('company', False)
        fiscalyear_id = fiscalyear_obj.find(company_id,
                date=entry_date, exception=False)
        bank_imp_config = bank_imp_config_obj.browse(
                line.bank_import_config.id)
        journal_id = bank_imp_config.journal.id

        res = {
            'batch': batch_id,
            'journal': journal_id,
            'date': entry_date,
            'invoice': invoice_id,
            'external_reference': invoice_number,
            'amount': amount,
            'party': party_id,
            'contra_account': contra_account,
            'tax': tax_id,
            'bank_imp_line': line.id,
            'fiscalyear': fiscalyear_id,
            }
        account_sides = self._get_account_and_sides(batch_id, entry_date,
            amount)
        res.update(account_sides)
        return res

    def _get_account_and_sides(self, batch_id, date, amount):
        pool = Pool()
        batch_obj = pool.get('account.batch')
        batch_journal_obj = pool.get('account.batch.journal')
        account_journal_obj = pool.get('account.journal')
        batch = batch_obj.browse(batch_id)
        batch_journal = batch_journal_obj.browse(batch.journal.id)
        account_journal = account_journal_obj.browse(
            batch_journal.account_journal.id)

        if amount >= _ZERO:
            account = account_journal.debit_account.id
            currency_digits = account_journal.debit_account.currency_digits
            side_account = 'debit'
            side_contra_account = 'credit'
        else:
            account = account_journal.credit_account.id
            currency_digits = account_journal.credit_account.currency_digits
            side_account = 'credit'
            side_contra_account = 'debit'

        res = {
            'side_account': side_account,
            'account': account,
            'side_contra_account': side_contra_account,
            'currency_digits': currency_digits,
            }
        return res

    def _get_tax(self, invoice_id, date):
        return False

    def _get_batch(self, config_id):
        '''
        Return values for a batch to insert banking import lines

        :param bank_imp_config: the id of the banking import configuration

        :return: a dictionary with
                keys: batch fields
                values: batch values
        '''
        pool = Pool()
        bank_imp_config_obj = pool.get('banking.import.configuration')
        batch_line_obj = pool.get('account.batch.line')
        lang_obj = pool.get('ir.lang')

        res = {}
        bank_imp_config = bank_imp_config_obj.browse([config_id])
        for config in bank_imp_config:
            if not (config.journal.account_journal.credit_account and
                        config.journal.account_journal.debit_account):
                self.raise_user_error('missing_journal_accounts')

            # for correct start balance check there may be no unposted
            # batch lines for same accounts
            account_ids = [config.journal.account_journal.credit_account.id,
                            config.journal.account_journal.debit_account.id]

            batch_lines = batch_line_obj.check_unposted_batch_lines_for_account(
                    account_ids)
            if batch_lines:
                res['name'] = 'unposted'
                return res

            code = Transaction().context.get('language') or 'en_US'
            lang_ids = lang_obj.search([
                    ('code', '=', code),
                    ])
            lang = lang_obj.browse(lang_ids[0])
            now = datetime.datetime.now().strftime(str(lang.date) + " %H:%M")
            res['journal'] = config.journal.id
            res['name'] =  'Bank Import ' + config.name + ' (' + now + ')'
            return res

BankingImportLine()


class BankingImportFile(ModelSQL, ModelView):
    'Bank Import File'
    _name = 'banking.import.file'
    _description = __doc__

    import_file_cache = fields.Binary('Bank Import File', states=_STATESF,
            required=True, depends=_DEPENDSF)
    bank_import_config = fields.Many2One('banking.import.configuration',
            'Bank Import Configuration', states=_STATESF,
            required=True, ondelete='RESTRICT', depends=_DEPENDSF,
            help='Bank Import Configuration to use for the selected file')
    date = fields.DateTime('Import Date', readonly=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('saved', 'Saved'),
            ('failed', 'Failed'),
            ('parsed', 'Parsed'),
            ('imported', 'Imported'),
            ('partial', 'Partially Assigned'),
            ('assigned', 'Assigned'),
            ], 'State', readonly=True, select=1)
    log = fields.Text('Log', readonly=True)
    employee = fields.Many2One('company.employee', 'Employee',
            states=_STATESF, required=True, depends=_DEPENDSF,
            select=1, domain=[
                ('company', '=', Get(Eval('context', {}), 'company', False))])

    def __init__(self):
        super(BankingImportFile, self).__init__()
        self._error_messages.update({
            'invalid_account': 'Invalid bank data for this configuration!',
            'invalid_currency': 'Invalid currency in parsed data!',
            'invalid_data': 'Missing required fields in parsed data!',
        })
        self._order.insert(0, ('log', 'DESC'))

    def init(self, module_name):
        super(BankingImportFile, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # Migration from 2.0 import_file renamed into import_file_cache
        # to remove base64 encoding

        if (table.column_exist('import_file')
                and table.column_exist('import_file_cache')):
            cursor.execute('SELECT id, import_file '
                'FROM "' + self._table + '"')
            for import_file_id, report in cursor.fetchall():
                if report:
                    report = buffer(base64.decodestring(str(report)))
                    cursor.execute('UPDATE "' + self._table + '" '
                        'SET import_file_cache = %s '
                        'WHERE id = %s', (report, import_file_id))
                table.drop_column('import_file')

    def default_state(self):
        return 'draft'

    def default_employee(self):
        user_obj = Pool().get('res.user')

        employee_id = None

        if Transaction().context.get('employee'):
            employee_id = Transaction().context['employee']
        else:
            user = user_obj.browse(Transaction().user)
            if user.employee:
                employee_id = user.employee.id
        if employee_id:
            return employee_id
        return False

    def run_import(self, values):
        file_id = self.create(values)
        parsed_lines = self.parse_data([file_id])
        if self.import_lines([file_id], parsed_lines):
            return file_id
        return False

    def parse_data(self, ids):
        for file in self.browse(ids):
            method_name = file.bank_import_config.import_method
            method = BankingImportMethod(method_name)
            data = str(file.import_file_cache)
            if HAS_CHARDET and file.bank_import_config.encoding == 'auto':
                enc = chardet.detect(data)['encoding']
                data = data.decode(enc)
            else:
                data = data.decode(file.bank_import_config.encoding)
            res = method.parse(file.bank_import_config.id, data)
            res[0]['log'] = file.log + '\n\n' + res[0]['log']
            self.write(file.id, res[0])

        return res[1]

    def import_lines(self, ids, values):
        '''
        A line import has to be strictly chronical, i.e. date and balance
        have to match the last existing import line.
        '''
        bank_imp_line_obj = Pool().get('banking.import.line')
        res = True

        for imp_file in self.browse(ids):
            imp_file_values = {
                'log': imp_file.log or \
                        'Import of data failed with unknown reason!',
                'state': 'failed',
                }
            # Proceed all preliminary checks and conversions
            if not self._check_required_data(values) \
                or not self._convert_currency(values) \
                or not self._check_line_account(values):
                self.write(imp_file.id, imp_file_values)
                res = False
                break
            succ = 0
            for line in values:
                # Check for chronical match criteria and for correct balance
                if not self._check_line_match(line):
                    continue
                bank_imp_line_obj.create(line)
                succ += 1
            # Log result
            rlog = '\n\nImport Results:\n---------------------\n\n' + \
                'Transactions Total: %s\n' % (len(values)) + \
                'New Transactions Imported: %s\n' % (succ) + \
                'Transactions Not Matched: %s\n' % (len(values) - succ)
            imp_file_values['log'] = imp_file.log + rlog
            imp_file_values['state'] = 'imported'
            self.write(imp_file.id, imp_file_values)

        return res

    def _convert_currency(self, values):
        '''
        Check and convert currency values
        '''
        currency_obj = Pool().get('currency.currency')

        for line in values:
            if line.get('currency'):
                currency_id = currency_obj.search([
                    ('code', '=', line['currency']),
                    ], limit=1)
                if currency_id:
                    line['currency'] = currency_id[0]
                else:
                    self.raise_user_error('invalid_currency')
                    return False
        return True

    def _check_line_account(self, values):
        '''
        Check for matching bank data
        '''
        pool = Pool()
        bank_imp_config_obj = pool.get('banking.import.configuration')
        bank_account_obj = pool.get('bank.account')
        bank_obj = pool.get('bank.bank')

        for line in values:
            if line.get('bank_import_config'):
                bank_config = bank_imp_config_obj.browse(
                        line['bank_import_config'])
                bank_account = bank_account_obj.browse(
                        bank_config.bank_account.id)
                bank = bank_obj.browse(bank_account.bank.id)
                account_nr = bank_account.code
                bank_code = bank.bank_code
                if line.get('account_recipient') and \
                    line.get('bank_code_recipient'):
                    if line['account_recipient'] != account_nr or \
                        line['bank_code_recipient'] != bank_code:
                        self.raise_user_error('invalid_account')
                        return False
                    return True
        return False

    def _check_line_match(self, line):
        '''
        Check for matching next import
        '''
        bank_imp_line_obj = Pool().get('banking.import.line')

        if line.get('bank_import_config'):
            imp_line_id = bank_imp_line_obj.search([
                ('bank_import_config', '=', line['bank_import_config']),
                ],
                order=[('id', 'DESC')], limit=1)
            if imp_line_id:
                imp_line = bank_imp_line_obj.browse(imp_line_id[0])
            else:
                return True
            if line.get('date') and line.get('start_balance'):
                if line['date'] >= imp_line.date and \
                        line['start_balance'] == imp_line.end_balance:
                    return True
        return False

    def _check_required_data(self, values):
        '''
        Check for minimal required data for a valid line
        '''
        for line in values:
            res = True
            if not (line.get('bank_import_config') and line.get('date') and
                line.get('valuta_date') and
                (line.get('account_recipient') or line.get('iban')) and
                (line.get('bank_code_recipient') or line.get('bic')) and
                line.get('currency')):
                res = False
            # Amounts can be zero and evaluate to False. Just check for
            # existence of those fields.
            if res:
                amount_fields = ['amount', 'start_balance', 'end_balance']
                for field in amount_fields:
                    if field not in line:
                        res = False
                        break
            if not res:
                self.raise_user_error('invalid_data')
            return res

    def match_invoice(self, line_id):
        '''
        Find matching invoices for the import line.

        Overload this member in submodules to suit specific needs,
        number formats, etc.

        Note:
        The more unique invoice numbers are the better will be matching.
        This simple matcher will produce many false positives in a tryton
        default configuration.

        :param line_id: the id of the Banking Import Line

        :returns: a list of invoice_ids
        '''
        invoice_obj = Pool().get('account.invoice')
        bank_imp_line_obj = Pool().get('banking.import.line')
        invoice_ids = []

        line = bank_imp_line_obj.browse(line_id)
        if line['purpose']:
            purpose = line['purpose'].replace('\n','')
            open_invoice_ids = invoice_obj.search([
                ('state', '=', 'open'),
                ], order=[('id', 'ASC')])
            if open_invoice_ids:
                open_invoices = invoice_obj.browse(open_invoice_ids)
                for invoice in open_invoices:
                    if purpose.find(invoice.number) > -1:
                        invoice_ids.append(invoice.id)
                invoice_ids.sort()
        return invoice_ids

    def ask_invoice(self, line_id, invoice_ids=None):
        '''
        Make plausibility checks for a specified invoice and return, whether the
        wizard should present the line for choosing or verifying the invoice.

        Overload this member in submodules to suit specific needs,
        number formats, etc.

        This example contains one plausibility check:
        - amount of the line must be equal to the total amount to pay of
          all matched invoices

        :param line_id: the id of the Banking Import Line
        :param invoice_ids: a list of invoice_ids

        :returns: a dict with
            keys: the invoice_ids
            values: the value as Boolean (True for ask)
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        bank_imp_line_obj = pool.get('banking.import.line')
        currency_obj = pool.get('currency.currency')

        res = {}
        if invoice_ids is None:
            return res
        plty = 0
        line = bank_imp_line_obj.browse(line_id)
        journal = line.bank_import_config.journal
        invoices = invoice_obj.browse(invoice_ids)
        invoice_sum = Decimal('0.0')
        for invoice in invoices:
            with Transaction().set_context(date=invoice.currency_date):
                amount_to_pay = currency_obj.compute(invoice.currency.id,
                        invoice.amount_to_pay, journal.currency.id)
            invoice_sum += amount_to_pay
        if invoice_sum == line.amount:
            plty += 1
        for invoice in invoices:
            if plty > 0:
                res[invoice.id] = False
            else:
                res[invoice.id] = True
        return res

BankingImportFile()


class BankingImportMethod(object):
    '''
    Inherit from this object to provide specific parsers by method
    '''

    def __new__(cls, name):
        for subclass in BankingImportMethod.__subclasses__():
            if subclass._name == name:
                return super(cls, subclass).__new__(subclass)
        raise Exception, 'Method not supported!'

    def parse(self, import_config, data):
        '''
        Parse data.

        data is a file stored as binary object in the DB.

        :param import_config: the id of the Import Configuration
        :param data: the data to be parsed

        :returns: a list containing
                - a dict with the data to update the record
                  of the Banking Import File with
                  key: field
                  value: value as string
                - a list of dicts with the data to be put
                  in Banking Import Lines with
                  key: field
                  value: value as string
        '''
        raise NotImplementedError


class RunImportInit(ModelView):
    'Run Import Init'
    _name = 'banking.run_import.init'
    _description = __doc__
    employee = fields.Many2One('company.employee', 'Employee', required=True,
            domain=[('company', '=', Eval('company'))], depends=['company'])
    bank_import_config = fields.Many2One('banking.import.configuration',
            'Bank Import Configuration', required=True, ondelete='RESTRICT',
            help='Bank Import Configuration to use for this run')
    import_file_cache = fields.Binary('Bank Import File',
            help='If an import file is provided, it will be parsed for new ' \
            'transactions for the selected configuration.')

    def default_employee(self):
        file_obj = Pool().get('banking.import.file')
        return file_obj.default_employee()

    def search(self, args, offset=0, limit=None, order=None,
            count=False, query_string=False):
        ''' Dummy search '''
        args = args[:]

RunImportInit()


class CreateBatchAskInvoice(ModelView):
    'Create Batch Ask Invoice'
    _name = 'banking.import.line.create_batch.ask_invoice'
    _description = __doc__

    bank_imp_file = fields.Many2One('banking.import.file', 'Bank Import File')
    counter = fields.Char('Counter', readonly=True,
            help='Display of the number of the actual ' \
            'and total banking import lines')
    account_recipient = fields.Char('Account Recipient', readonly=True)
    bank_code_recipient = fields.Char('Bank Code Recipient', readonly=True)
    date = fields.Date('Date', readonly=True)
    valuta_date = fields.Date('Valuta Date', readonly=True)
    account_payer = fields.Char('Account Payer', readonly=True)
    bank_code_payer = fields.Char('Bank Code Payer', readonly=True)
    amount = fields.Numeric('Amount', readonly=True,
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'])
    currency = fields.Char('Currency', readonly=True)
    posting_text = fields.Char('Posting Text', readonly=True)
    names = fields.Char('Names', readonly=True)
    start_balance = fields.Numeric('Start Balance', readonly=True,
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'])
    end_balance = fields.Numeric('End Balance', readonly=True,
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits'])
    purpose = fields.Text('Purpose', readonly=True)
    batch_import_lines = fields.One2Many('account.batch.import.line', 'batch',
        'Batch Import Lines', on_change=[
            'batch_import_lines', 'batch', 'bank_imp_line', 'difference',
            'amount'
            ])
    sum_lines = fields.Function(fields.Numeric('Sum Lines',
        digits=(16, Eval('currency_digits', 2)), readonly=True,
        depends=['currency_digits', 'batch_import_lines'],
        help='This field displays the sum of the actual encoded lines.'),
        'get_function_fields')
    difference = fields.Function(fields.Numeric('Difference',
        digits=(16, Eval('currency_digits', 2)), readonly=True,
        depends=['currency_digits'],
        help='This field displays the difference to the expected balance.'),
        'get_function_fields')
    batch = fields.Many2One('account.batch', 'Batch')
    bank_imp_line = fields.Many2One('banking.import.line', 'Bank Import Line')
    currency_digits = fields.Function(fields.Integer('Currency Digits',
        on_change_with=['batch']), 'get_currency_digits')
    balanced = fields.Char('Balanced', required=True,
        help='Helper field for wizard logic')

    def default_get(self, fields):
        company_obj = Pool().get('company.company')
        res = {}
        batch = Transaction().context.get('batch')
        batch_import_lines = Transaction().context.get('batch_import_lines')
        if 'batch' in fields:
            res['batch'] = batch
        if 'currency' in fields:
            res['currency_digits'] = 2
            if Transaction().context.get('company'):
                company = company_obj.browse(Transaction().context['company'])
                res['currency_digits'] = company.currency.digits
        # Get inital values for the first update of the form from
        # on_change_batch_import_lines, it is not called by default
        values = {
            'batch': batch,
            'batch_import_lines': batch_import_lines
            }
        res2 = self.on_change_batch_import_lines(values)
        if 'sum_lines' in fields:
            res['sum_lines'] = res2.get('sum_lines')
        if 'difference' in fields:
            res['difference'] = res2.get('difference')
        if 'bank_imp_line' in fields:
            res['bank_imp_line'] = res2.get('bank_imp_line')
        if 'balanced' in fields:
            res['balanced'] = res2.get('balanced')
        return res

    def on_change_batch_import_lines(self, values):
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.batch.journal')
        currency_obj = pool.get('currency.currency')
        batch_obj = pool.get('account.batch')
        bank_imp_line_obj = pool.get('banking.import.line')
        batch_imp_line_obj = pool.get('account.batch.import.line')
        bank_imp_config_obj = pool.get('banking.import.configuration')

        res = {
            'sum_lines': Decimal('0.0'),
            'difference': Decimal('0.0'),
        }

        batch_import_lines = values.get('batch_import_lines', {})
        if not values.get('batch') or not batch_import_lines:
            # Needed to be able to quit the wizard in case all lines are
            # deleted. Possible drawback: it is also possible to continue and
            # skip bank import lines
            res['balanced'] = 'Yes'
            res['difference'] = values.get('amount')
            return res

        res['batch_import_lines'] = {}
        batch = batch_obj.browse(values['batch'])
        journal = journal_obj.browse(batch.journal.id)
        invoice_ids = set()
        balanced = False
        check_date = True
        check_contra_account = True
        # Workaround #400:
        # establish defaults to be always able to update/create lines
        defaults = {
                    'batch': batch.id,
                    'journal': journal.id,
                    'bank_imp_line': values.get('bank_imp_line')
                    }
        for line in batch_import_lines:
            if not defaults['bank_imp_line'] and line.get('bank_imp_line'):
                defaults['bank_imp_line'] = line['bank_imp_line']
            res['sum_lines'] += line['amount']
            if line.get('invoice'):
                invoice_ids.add(line['invoice'])
            if check_date and not line.get('date'):
                check_date = False
            if check_contra_account and not line.get('contra_account'):
                check_contra_account = False

        res['bank_imp_line'] = defaults['bank_imp_line']

        bank_imp_line = bank_imp_line_obj.browse(
                defaults.get('bank_imp_line'))
        date = bank_imp_config_obj._get_entry_date(bank_imp_line)
        amount = bank_imp_line.amount
        res['difference'] = amount - res['sum_lines']

        # block/unblock wizard
        if currency_obj.is_zero(journal.currency, res['difference']):
            balanced = 'Yes'

        inv_ids2vals = {}
        invoice_ids = list(invoice_ids)
        for invoice in invoice_obj.browse(invoice_ids):
            inv_ids2vals[invoice.id] = {}
            with Transaction().set_context(date=invoice.currency_date):
                amount_to_pay = currency_obj.compute(
                    invoice.currency.id, invoice.amount_to_pay,
                    journal.currency.id)
            if invoice.type in ['in_credit_note', 'out_credit_note']:
                amount_to_pay = amount_to_pay * -1
            inv_ids2vals[invoice.id]['amount_to_pay'] = amount_to_pay
            inv_ids2vals[invoice.id]['party'] = invoice.party.id
            contra_account = batch_imp_line_obj._get_invoice_account_id(
                    invoice.account.id, date)
            inv_ids2vals[invoice.id]['contra_account'] = contra_account

        for line in batch_import_lines:
            if line.get('invoice') and line.get('id'):
                amount_to_pay = \
                            inv_ids2vals[line['invoice']]['amount_to_pay']
                party = inv_ids2vals[line['invoice']]['party']
                contra_account = \
                            inv_ids2vals[line['invoice']]['contra_account']
                if abs(line['amount']) > abs(amount_to_pay):
                    res['batch_import_lines'].setdefault('update', [])
                    if currency_obj.is_zero(
                            journal.currency, amount_to_pay):
                        res['batch_import_lines']['update'].append({
                            'id': line['id'],
                            'invoice': False,
                            'bank_imp_line': defaults['bank_imp_line'],
                            })
                    else:
                        res['batch_import_lines']['update'].append({
                            'id': line['id'],
                            'amount': amount_to_pay,
                            'party': party,
                            'contra_account': contra_account,
                            'bank_imp_line': defaults['bank_imp_line'],
                            })
                        res['batch_import_lines'].setdefault('add', [])
                        vals = line.copy()
                        del vals['id']
                        vals['amount'] = \
                                    line['amount'] - amount_to_pay
                        vals['invoice'] = False
                        vals['external_reference'] = False
                        res['batch_import_lines']['add'].append(vals)
            elif line.get('id'):
                res['batch_import_lines'].setdefault('update', [])
                res['batch_import_lines']['update'].append({
                        'id': line['id'],
                        'batch': defaults['batch'],
                        'journal': defaults['journal'],
                        'bank_imp_line': defaults['bank_imp_line'],
                        })
        if not check_date or not check_contra_account:
            balanced = False
        res['balanced'] = balanced
        return res

    def on_change_with_currency_digits(self, values):
        batch_obj = Pool().get('account.batch')

        if values.get('batch'):
            batch = batch_obj.browse(values['batch'])
            return batch.currency_digits
        return 2

    def get_currency_digits(self, ids, name):
        '''
        Return the number of digits of the currency for each transaction

        :param ids: a list of transaction ids
        :return: a dictionary with transaction id as key and
            number of digits as value
        '''
        res = {}
        for transaction in self.browse(ids):
            res[transaction.id] = transaction.batch.currency_digits
        return res

CreateBatchAskInvoice()


class RunImportAskBalance(ModelView):
    'Run Import Info'
    _name = 'banking.run_import.ask_balance'
    _description = __doc__

RunImportAskBalance()


class RunImportInfoUnposted(ModelView):
    'Run Import Info Unposted'
    _name = 'banking.run_import.info_unposted'
    _description = __doc__

RunImportInfoUnposted()


class RunImportInfo(ModelView):
    'Run Import Info'
    _name = 'banking.run_import.info'
    _description = __doc__

RunImportInfo()


class RunImport(Wizard):
    'Run Import'
    _name = 'banking.run_import'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'banking.run_import.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('choice_file_provided', 'Run', 'tryton-ok', True),
                    ],
                },
            },
        'choice_file_provided': {
            'result': {
                'type': 'choice',
                'next_state': '_choice_file_provided',
                },
            },
        'choice_file_import': {
            'actions': ['_action_run_import_file'],
            'result': {
                'type': 'choice',
                'next_state': '_choice_file_import',
                },
            },
        'create_batch': {
            'result': {
                'type': 'choice',
                'next_state': '_create_batch',
                },
            },
        'ask_balance': {
            'result': {
                'type': 'form',
                'object': 'banking.run_import.ask_balance',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('create_batch', 'Continue', 'tryton-ok', True),
                    ],
                },
            },
        'ask_invoice': {
            'actions': ['_set_batch_line'],
            'result': {
                'type': 'form',
                'object': 'banking.import.line.create_batch.ask_invoice',
                'state': [
                    ('choice_cancel', 'Cancel', 'tryton-cancel'),
                    ('create_batch', 'Continue', 'tryton-go-next', True),
                    ],
                },
            },
        'choice_cancel': {
            'result': {
                'type': 'choice',
                'next_state': '_choice_cancel',
                },
            },
        'info_unposted': {
            'result': {
                'type': 'form',
                'object': 'banking.run_import.info_unposted',
                'state': [
                    ('open_unposted', 'Ok', 'tryton-ok', True),
                    ],
                },
            },
        'info_open': {
            'result': {
                'type': 'form',
                'object': 'banking.run_import.info',
                'state': [
                    ('open_file', 'Ok', 'tryton-ok', True),
                    ],
                },
            },
        'info_end': {
            'result': {
                'type': 'form',
                'object': 'banking.run_import.info',
                'state': [
                    ('end', 'Ok', 'tryton-ok', True),
                    ],
                },
            },
        'open_unposted': {
            'result': {
                'type': 'action',
                'action': '_action_open_unposted',
                'state': 'end',
                },
            },
        'open_file': {
            'result': {
                'type': 'action',
                'action': '_action_open_file',
                'state': 'end',
                },
            },
        'choice_end': {
            'result': {
                'type': 'choice',
                'next_state': '_choice_end',
                },
            },
        'open_batch': {
            'result': {
                'type': 'action',
                'action': '_action_open_batch',
                'state': 'end',
                },
            },
        }

    def _action_run_import_file(self, data):
        lang_obj = Pool().get('ir.lang')
        bank_imp_file_obj = Pool().get('banking.import.file')

        code = Transaction().context.get('language') or 'en_US'
        lang_ids = lang_obj.search([
                ('code', '=', code),
                ])
        lang = lang_obj.browse(lang_ids[0])

        now = datetime.datetime.now()
        str_datetime = now.strftime(str(lang.date) + ' %H:%M:%S')

        values = {
                'employee': data['form']['employee'],
                'import_file_cache': data['form']['import_file_cache'],
                'bank_import_config': data['form']['bank_import_config'],
                'log': 'File saved at %s' % (str_datetime),
                'state': 'saved',
                'date': now,
                }
        imp_file_id = bank_imp_file_obj.run_import(values)
        data['bank_imp_file_id'] = imp_file_id
        return {}

    def _choice_file_provided(self, data):
        # define here to be persistent in later wizards
        data['bank_imp_lines'] = []
        data['bank_imp_line2invoice'] = {}
        data['bank_import_config_id'] = data['form']['bank_import_config']

        if not data['form']['import_file_cache']:
            data['bank_imp_file_id'] = None
            return 'create_batch'
        return 'choice_file_import'

    def _choice_file_import(self, data):
        if not data['bank_imp_file_id']:
            return 'open_file'
        return 'create_batch'

    def _choice_end(self, data):
        batch_line_obj = Pool().get('account.batch.line')
        bank_imp_file_obj = Pool().get('banking.import.file')

        if data['batch_id'] and data['bank_imp_file_id']:
            lines = batch_line_obj.search([
                ('batch', '=', data['batch_id'])
                ])
            if lines:
                rlog = '\n\nBatch Results:\n---------------------\n\n' + \
                    'Transactions were succesfully written to batch.\n' + \
                    'Batched Transactions: %s\n' % len(lines)
                bank_imp_file = bank_imp_file_obj.browse(
                    data['bank_imp_file_id'])
                res_file = {
                        'log': bank_imp_file.log + rlog,
                        'state': 'assigned',
                        }
                bank_imp_file_obj.write(bank_imp_file.id, res_file)
        return 'open_batch'


    def _choice_cancel(self, data):
        # if wizard was canceled without creating any line:
        # cancel and delete batch and show file, else show batch
        pool = Pool()
        batch_obj = pool.get('account.batch')
        batch_line_obj = pool.get('account.batch.line')
        bank_imp_file_obj = pool.get('banking.import.file')

        res = {}
        if data['batch_id']:
            lines = batch_line_obj.search([
                ('batch', '=', data['batch_id'])
                ], limit=1)
            if lines:
                if data['bank_imp_file_id']:
                    rlog = '\n\nBatch Results:\n---------------------\n\n' + \
                        'Batch action canceled.\n' + \
                        'Transactions were partially written to batch.\n' + \
                        'Batched Transactions: %s\n' % len(lines)
                    bank_imp_file = bank_imp_file_obj.browse(
                        data['bank_imp_file_id'])
                    res['log'] = bank_imp_file.log + rlog
                    res['state'] = 'partial'
                    bank_imp_file_obj.write(bank_imp_file.id, res)
                return 'open_batch'
            else:
                batch_obj.delete(data['batch_id'])
                if data['bank_imp_file_id']:
                    rlog = '\n\nBatch Results:\n---------------------\n\n' + \
                        'Batch action canceled.\n' + \
                        'No Transactions were written to batch.'
                    bank_imp_file = bank_imp_file_obj.browse(
                            data['bank_imp_file_id'])
                    res['log'] = bank_imp_file.log + rlog
                    bank_imp_file_obj.write(bank_imp_file.id, res)
                    return 'info_open'
                return 'info_end'
        return 'end'

    def _create_batch(self, data):
        pool = Pool()
        bank_imp_config_obj = pool.get('banking.import.configuration')
        bank_imp_line_obj = pool.get('banking.import.line')
        bank_imp_file_obj = pool.get('banking.import.file')
        batch_user_obj = pool.get('account.batch.user')
        batch_obj = pool.get('account.batch')
        batch_line_obj = pool.get('account.batch.line')
        invoice_obj = pool.get('account.invoice')
        currency_obj = pool.get('currency.currency')

        # write batch_import_lines to batch_lines
        if data['form'].get('batch_import_lines'):
            for line in data['form']['batch_import_lines']:
                if line[0] in ['delete', 'add']:
                    continue
                batch_line = line[1]
                # remove fields not existing on batch_lines
                for field in ['currency_digits']:
                    if field in batch_line:
                        del batch_line[field]
                if 'batch' not in batch_line:
                    batch_line['batch'] = data['batch_id']
                batch_line_obj.create(batch_line)

        # only on first run of this wizard:
        # - create batch
        # - save batch for user to have a global reference for batch_line_import
        # - fetch all lines to handle once into data
        if not data['bank_imp_lines']:
            bank_imp_config = bank_imp_config_obj.browse(
                    data['bank_import_config_id'])

            # try first to create batch
            values = bank_imp_line_obj._get_batch(bank_imp_config.id)
            if values['name'] == 'unposted':
                data['bank_import_config'] = data['form']['bank_import_config']
                return 'info_unposted'
            batch_id = batch_obj.create(values)
            data['batch_id'] = batch_id

            # save batch-user for global reference
            batch_user_ids = batch_user_obj.search([
                    ('user',  '=',  Transaction().user),
                    ])
            if batch_user_ids:
                batch_user_obj.delete( batch_user_ids)
            user_id = Transaction().user
            batch_user_obj.create({
                    'user': user_id,
                    'batch': batch_id,
                    })
            # search all import lines. that are not referenced by any posted
            # batch line of this journal
            # exclude amounts of 0, they have no posted move lines
            journal_id = bank_imp_config.journal.id
            cursor = Transaction().cursor
            cursor.execute('SELECT il.id ' \
                        'FROM banking_import_line il ' \
                        'INNER JOIN banking_import_configuration ic ' \
                        'ON (il.bank_import_config = ic.id) ' \
                        'WHERE il.id NOT IN ' \
                        '(SELECT DISTINCT bl.bank_imp_line ' \
                            'FROM account_batch_line bl ' \
                            'INNER JOIN account_move m ' \
                            'ON (m.id = bl.move) ' \
                            'WHERE bl.journal = %s ' \
                            'AND m.state = \'posted\' ' \
                            'AND bl.bank_imp_line IS NOT NULL) ' \
                        'AND il.amount <> 0 ' \
                        'AND ic.journal = %s ' \
                        'ORDER BY il.id', (journal_id, journal_id,))

            imp_line_ids = cursor.fetchall()
            data['counter_total'] = len(imp_line_ids)

            if not imp_line_ids:
                batch_obj.delete(data['batch_id'])
                if data['form']['import_file_cache']:
                    bank_imp_file = bank_imp_file_obj.browse(
                            data['bank_imp_file_id'])
                    rlog = '\n\nJournal Results:\n---------------------\n\n' + \
                        'No transactions for journal import found.'
                    res = {'log': bank_imp_file.log + rlog}
                    bank_imp_file_obj.write(data['bank_imp_file_id'], res)
                    return 'info_open'
                else:
                    return 'info_end'

            bank_imp_lines = bank_imp_line_obj.browse(
                    [x[0] for x in imp_line_ids])
            data['bank_imp_lines'] = bank_imp_lines

            # compare initial balance of batch with first import line
            account_id = bank_imp_config.journal.account_journal.debit_account.id
            start_balance = self._get_account_balance(account_id,
                date=bank_imp_lines[0].date)
            if start_balance != bank_imp_lines[0].start_balance:
                return 'ask_balance'

        # loop over lines in data to get invoice_ids
        res =  {}
        for bank_imp_line in data['bank_imp_lines']:
            # TODO Remove this with #1885
            line = bank_imp_line_obj.browse(bank_imp_line.id)
            # skip already seen lines
            if line.id in data['bank_imp_line2invoice']:
                continue
            data['bank_imp_line'] = line.id
            invoice_ids = bank_imp_file_obj.match_invoice(line.id)
            data['bank_imp_line2invoice'][line.id] = invoice_ids
            ask = False
            if not invoice_ids:
                ask = True
            else:
                invoices_to_ask = bank_imp_file_obj.ask_invoice(line.id,
                        invoice_ids)
                for invoice in invoices_to_ask.values():
                    if invoice:
                        ask = True
            # get invoice amounts
            invoice_id2amount_to_pay = {}
            for invoice in invoice_obj.browse(invoice_ids):
                amount_to_pay = invoice.amount_to_pay
                if invoice.type in ['in_credit_note', 'out_credit_note']:
                    amount_to_pay = amount_to_pay * -1
                with Transaction().set_context(date=invoice.currency_date):
                    invoice_id2amount_to_pay[invoice.id] = currency_obj.compute(
                        invoice.currency.id, amount_to_pay, line.currency.id)

            # create lines with invoice_ids found
            batch_import_lines = []
            running_amount = line.amount
            for invoice_id in invoice_ids:
                amount_to_pay = invoice_id2amount_to_pay[invoice_id]
                ask_invoice = False
                if abs(running_amount) < abs(amount_to_pay):
                    amount_paid = running_amount
                    ask_invoice = True
                else:
                    amount_paid = amount_to_pay
                values = bank_imp_line_obj._get_batch_import_line(line,
                        data['batch_id'], amount_paid, invoice_id)
                batch_import_lines.append(values)
                if ask_invoice:
                    data['batch_import_lines'] = batch_import_lines
                    self._set_counter(data)
                    return 'ask_invoice'
                running_amount -= amount_to_pay

            currency = currency_obj.browse([line.currency.id])
            if not currency_obj.is_zero(currency[0], running_amount):
                ask = True
                values = bank_imp_line_obj._get_batch_import_line(line,
                        data['batch_id'], running_amount, False)
                batch_import_lines.append(values)
            data['batch_import_lines'] = batch_import_lines
            self._set_counter(data)

            if ask:
                return 'ask_invoice'
            else:
                for batch_imp_line in batch_import_lines:
                    # remove fields not existing on batch_lines
                    for field in ['currency_digits']:
                        if field in batch_imp_line:
                            del batch_imp_line[field]
                    batch_line_obj.create(batch_imp_line)
                data['batch_import_lines'] = None
                data['form']['batch_import_lines'] = None
                return 'create_batch'
        return 'choice_end'

    def _get_account_balance(self, account_id, date=None):
        pool = Pool()
        batch_obj = pool.get('account.batch')
        return batch_obj._calc_start_balance(account_id)

    def _set_counter(self, data):
        if data.get('counter_actual'):
            data['counter_actual'] += 1
        else:
            data['counter_actual'] = 1
        data['counter'] = ' / '.join([str(data['counter_actual']),
            str(data['counter_total'])])

    def _set_batch_line(self, data):
        bank_imp_line_obj = Pool().get('banking.import.line')

        line_id = data['bank_imp_line']
        bank_imp_line = bank_imp_line_obj.browse(line_id)

        # Those two are needed in CreateBatchAskInvoice.default_get
        # for a first validation of the form
        Transaction().set_context(
                batch_import_lines=data['batch_import_lines'])
        Transaction().set_context(batch=data['batch_id'])

        values = {
            'account_recipient': bank_imp_line.account_recipient,
            'bank_code_recipient': bank_imp_line.bank_code_recipient,
            'date': bank_imp_line.date,
            'valuta_date': bank_imp_line.valuta_date,
            'account_payer': bank_imp_line.account_payer,
            'bank_code_payer': bank_imp_line.bank_code_payer,
            'amount': bank_imp_line.amount,
            'currency': bank_imp_line.currency.code,
            'posting_text': bank_imp_line.posting_text,
            'names': bank_imp_line.names,
            'purpose': bank_imp_line.purpose,
            'start_balance': bank_imp_line.start_balance,
            'end_balance': bank_imp_line.end_balance,
            'batch': data['batch_id'],
            'counter': data['counter'],
            'bank_imp_line': line_id,
            'batch_import_lines': data['batch_import_lines'],
            }
        return values

    def _action_open_unposted(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        bank_imp_config_obj = pool.get('banking.import.configuration')

        if not data.get('bank_import_config'):
            return 'end'
        bank_imp_config = bank_imp_config_obj.browse(
                data['bank_import_config'])
        journal_id = bank_imp_config.journal.id

        act_window_id = model_data_obj.get_id('account_batch',
                'act_batch_line_form_editable')
        res = act_window_obj.read(act_window_id)
        # Use name from view_header_get
        del res['name']

        domain = [
                ('journal', '=', journal_id),
                ('move.state', '!=', 'posted'),
                ]
        ctx = {
                'journal': journal_id,
                }
        res['domain'] = str(domain)
        res['context'] = str(ctx)
        res['pyson_domain'] = PYSONEncoder().encode(domain)
        res['pyson_context'] = PYSONEncoder().encode(ctx)
        return res

    def _action_open_file(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        employee_obj = pool.get('company.employee')
        model_data_ids = model_data_obj.search( [
            ('fs_id', '=', 'act_bank_import_file_form'),
            ('module', '=', 'account_banking_import'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        if data['form'].get('employee'):
            res['pyson_domain'] = PYSONEncoder().encode([
                ('employee', '=', data['form']['employee']),
                ])
            res['pyson_context'] = PYSONEncoder().encode({
                'employee': data['form']['employee'],
                })
            employee = employee_obj.browse(data['form']['employee'])
            res['name'] += " - " + employee.rec_name
        return res

    def _action_open_batch(self, data):
        pool = Pool()
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        batch_obj = pool.get('account.batch')

        act_window_id = model_data_obj.get_id('account_batch',
                'act_batch_line_form_editable')
        res = act_window_obj.read(act_window_id)
        # Use name from view_header_get
        del res['name']

        batch_id = data['batch_id']
        batch = batch_obj.browse(batch_id)
        journal_id = batch.journal.id
        domain = [
                ('journal', '=', journal_id),
                ('batch', '=', batch_id),
                ]
        ctx = {
                'journal': journal_id,
                'batch': batch_id,
                }
        res['domain'] = str(domain)
        res['context'] = str(ctx)
        res['pyson_domain'] = PYSONEncoder().encode(domain)
        res['pyson_context'] = PYSONEncoder().encode(ctx)
        return res

RunImport()

class BatchUser(ModelSQL, ModelView):
    'Account Batch User'
    _name = 'account.batch.user'
    _description = __doc__
    _rec_name = 'description'

    batch = fields.Many2One('account.batch', 'Batch',
            required=True, ondelete='CASCADE', readonly=True,  )
    user = fields.Many2One('res.user', 'User',
            required=True, ondelete='CASCADE', readonly=True, select=1)

BatchUser()

