Banking Import Module
#####################

This document describes the general concepts of the account_banking_import
module. To get help the mouse can be positioned over a widget to show
appropriate tool-tips.

This module
    - provides the import of transactions from bank accounts
    - provides reconciliation of payments with invoices
    - provides the creation of reconciliation drafts in an automatic way

This module makes usage of import methods, that are defined in other
submodules. So this module needs the additional installation of at least one
import method module to be able to work.


Configuration
=============

Important Note
--------------

The more unique invoice numbers are, the better will be automatic matching.
The simple matcher included in this module will produce many false positives
in a tryton default configuration. It is essential to configure the invoice
sequences to contain unique elements.
In the same way the included plausibilty checker should be overriden by some
custom module to provide more reliability (currently it is only based on the
check of the invoice amount).

Entry date for accounting
-------------------------

Bank statements usually come with two dates: date and valuta date. Usually the
valuta date should be the one indicating the real valutation date of an amount,
i.e. when the amount will be considered to be really available (interests,
exchange rates etc.). However the usage of the valuta date by the
banking institutions seems not to be standardized, thus producing problems in
the sequence of the accounting import statements.
To avoid those problems and to follow the consecutive sequence of bank
statements the normal (booking) date is used as default entry date for
accounting. If you wish to use rather the valuta date, adapt the configuration
of the relative banking configuration.

