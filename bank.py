#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.transaction import Transaction
from trytond.pool import Pool


class BankAccount(ModelSQL, ModelView):
    _name = 'bank.account'

    def __init__(self):
        super(BankAccount, self).__init__()
        self.currency = copy.copy(self.currency)
        if not self.currency.states:
            self.currency.states = {'required': True}
        else:
            self.currency.states['required'] = True
        self._reset_columns()

    def default_party(self):
        company_obj = Pool().get('company.company')
        if (Transaction().context.get('company_bank')
                and Transaction().context.get('company')):
            company = company_obj.browse(Transaction().context['company'])
            return company.party.id
        return False

BankAccount()
